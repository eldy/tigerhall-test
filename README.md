This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`

to start the app, open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test:integration`

to test with cypress
