/// <reference types="cypress" />

describe("test homepage", () => {
  beforeEach(() => {
    cy.visit("http://localhost:3000");
  });

  it("show cards on initial load", () => {
    cy.intercept("POST", "/graphql", {
      fixture: "contentCardsResp.json",
    });
    cy.get("[data-cy=card]").should("have.length", 5);
  });

  it("show filtered cards on search", () => {
    const searchKeyword = "startup";

    cy.get("[data-cy=searchInput]").type(searchKeyword);

    cy.intercept("POST", "/graphql", {
      fixture: "contentCardsRespSearchedKeyword.json",
    });

    cy.get("[data-cy=card]")
      .find("h1")
      .contains(searchKeyword, { matchCase: false });
  });
});
