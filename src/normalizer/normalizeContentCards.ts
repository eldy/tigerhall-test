import { ContentCardsData } from "../hooks/useContentCards";

const insertAt = (str: string, sub: string, pos: number) =>
  `${str.slice(0, pos)}${sub}${str.slice(pos)}`;

const normalizeContentCards = (contentCardList: ContentCardsData[]) =>
  contentCardList.map((item, i) => {
    const expert = item.experts?.[0];

    //8 is the length of https://
    const hostnameLength = item.image.uri.split("/")[2].length + 8;

    return {
      image: insertAt(item.image.uri, "/resize/250x", hostnameLength),
      courseName: item.name,
      category: item.categories.map(({ name }) => name).join(","),
      expertName: `${expert.firstName} ${expert.lastName}`,
      expertTitle: expert.title,
      company: expert.company,
    };
  });

export default normalizeContentCards;
