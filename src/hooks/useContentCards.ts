import { useQuery } from "react-query";
import { request, gql } from "graphql-request";

export interface ContentCardsData {
  name: string;
  image: {
    uri: string;
  };
  categories: {
    name: string;
  }[];
  experts: {
    firstName: string;
    lastName: string;
    title: string;
    company: string;
  }[];
}

const useContentCards = (keywords: string) =>
  useQuery("contentCards", async () => {
    const result = await request(
      "https://api.staging.tigerhall.io/graphql",
      gql`
        query ($keywords: String) {
          contentCards(
            filter: { limit: 5, keywords: $keywords, types: [PODCAST] }
          ) {
            edges {
              ... on Podcast {
                name
                image {
                  ...Image
                }
                categories {
                  ...Category
                }
                experts {
                  ...Expert
                }
              }
            }
          }
        }
        fragment Image on Image {
          uri
        }
        fragment Category on Category {
          name
        }
        fragment Expert on Expert {
          firstName
          lastName
          title
          company
        }
      `,
      {
        keywords,
      }
    );

    return result.contentCards.edges as ContentCardsData[];
  });

export default useContentCards;
