import React from "react";
import styled from "styled-components";

interface SearchInputProps extends React.HTMLProps<HTMLInputElement> {}

const Wrap = styled.div`
  p {
    font-weight: bold;
    color: #ffffff;
    margin: 10px auto 2px auto;
  }

  input {
    padding: 6px;
    width: 100%;
    margin-bottom: 20px;
    border-radius: 5px;
    background: ${(props) => props.theme.colors.teal};
    border: none;
    font-size: 16px;
    color: #ffffff;
  }
`;

const SearchInput: React.FC<SearchInputProps> = ({ ...props }) => {
  return (
    <Wrap>
      <p>Search</p>
      <input
        data-cy="searchInput"
        type="text"
        placeholder="Type any keyword"
        {...props}
      />
    </Wrap>
  );
};

export default SearchInput;
