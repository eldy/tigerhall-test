import React from "react";
import styled from "styled-components";
import Skeleton from "react-loading-skeleton";
import "react-loading-skeleton/dist/skeleton.css";

const Card = styled.div`
  background: #ffffff;
  margin: 0 auto 12px auto;
  border-radius: 5px;
`;

const ImageCover = styled.div<{ bg: string }>`
  height: 180px;
  background: url(${(props) => props.bg}) no-repeat center center;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  width: 100%;
  background-size: cover;
`;

const Content = styled.div`
  padding: 10px 20px 20px 20px;

  p {
    margin: 0;
    color: ${(props) => props.theme.colors.orange};
    font-size: 14px;

    &:nth-of-type(1) {
      font-weight: 500;
    }
  }

  h1 {
    font-size: 16px;
    margin: 2px 0 4px 0;
  }

  h2,
  h4 {
    font-size: 14px;
    margin: 0;
    font-weight: 500;
  }
`;

export interface ContentCardProps {
  image: string;
  courseName: string;
  category: string;
  expertName: string;
  expertTitle: string;
  company: string;
}

const ContentCard: React.FC<{ data: ContentCardProps; isLoading: Boolean }> = ({
  data: { image, courseName, category, expertName, expertTitle, company },
  isLoading,
}) => {
  return (
    <Card key={courseName} data-cy="card">
      {isLoading ? (
        <Skeleton
          style={{
            height: "180px",
          }}
        />
      ) : (
        <ImageCover bg={image}></ImageCover>
      )}
      <Content>
        {isLoading ? (
          <Skeleton count={5} />
        ) : (
          <>
            <p>{category}</p>
            <h1>{courseName}</h1>
            <h2>{expertName}</h2>
            <h4>{expertTitle}</h4>
            <p>{company}</p>
          </>
        )}
      </Content>
    </Card>
  );
};

export default ContentCard;
