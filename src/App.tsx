import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDebounce } from "use-debounce";
import "./App.css";
import { SearchInput, ContentCard } from "./Components";
import normalizeContentCards from "./normalizer/normalizeContentCards";
import useContentCards from "./hooks/useContentCards";

const Container = styled.div`
  background: #000000;
  padding: 6px 12px 20px 0;
  min-height: 100vh;
`;

const InnerContainer = styled.div`
  margin: 0 auto;
  max-width: 340px;
`;

function App() {
  const [searchValue, setSearchValue] = useState<string>("");
  const [debouncedsearchValue] = useDebounce(searchValue, 300);

  const { isFetching, data, refetch } = useContentCards(debouncedsearchValue);

  useEffect(() => {
    refetch();
  }, [debouncedsearchValue, refetch]);

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    setSearchValue(value);
  };

  return (
    <Container>
      <InnerContainer>
        <SearchInput onChange={handleOnChange} />
        {normalizeContentCards(data || []).map((d) => (
          <ContentCard key={d.courseName} isLoading={isFetching} data={d} />
        ))}
      </InnerContainer>
    </Container>
  );
}

export default App;
